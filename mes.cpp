#include<iostream>
#include<cmath>
#include<cstdlib>
#include<cstdio>
using namespace std;

double shift(double x){
	return 5*(1-x);
}

struct Matrix{
    int m,n;
    double **v;
    Matrix(int m, int n){
        this->m = m;
        this->n = n;
        this->v = new double*[m];
        for(int i=0; i<m; i++){
            this->v[i] = new double[n];
            for(int j=0; j<n; j++)
                this->v[i][j] = 0;
        }
    }
};


Matrix *zeros(int m, int n){
    Matrix *res = new Matrix(m,n);
    return res;
}


void printMatrix(Matrix *m){
    for(int i=0; i<m->m; i++){
        for(int j=0; j<m->n; j++){
            //cout<<m->v[i][j]<<"\t";
            printf("%.4f\t",m->v[i][j]);
        }
        cout<<endl;
    }
}


double e(int i, int n, int d, double x){
	double mini = (i-2.0)/n;
	double mid = mini + 1.0/n;
	double maxi = mid + 1.0/n;
	if(x<mini or x>maxi){
		return 0;
	} 
	
	// x in range (min, max)
	if(x < mid){
		if(d == 0) return x - mini;
		
		//d == 1
		return 1;
	}
	
	//x > mid
	if(d == 0) return maxi - x;
	
	//d == 1
	return -1;
}

double eps = 1e-3;

int main(int argc, char** argv){
	int n;
	double k;
	bool verbose = true;
	if(argc >= 3){
		verbose = false;
		k = atof(argv[1]);
		n = atoi(argv[2]);
	}
	
	if(verbose){
		cout<<"Podaj k: ";
		cin>>k;
		cout<<"Podaj n: ";
		cin>>n;
	}
	
	Matrix *S = zeros(n+1, n+1);
	Matrix *F = zeros(n+1, 1);
	
	//fill stiffness matrix
	for(int i=0;i<n+1; i++){
		for(int j=0; j<n+1; j++){
			//do not integrate trivial cases
			if(abs(i-j)>1) continue;
			
			//integrate
			double x = eps/2;
			double auxResult = 0;
			while(x < 1){
				S->v[i][j] += e(j+1, n, 1, x)*e(i+1, n, 0, x)*eps;
				auxResult += e(j+1, n, 1, x)*e(i+1, n, 1, x)*eps;	//v todo multiply by k
				
				x += eps;
			}
			S->v[i][j] += k*auxResult;								//^ done
		}
	}
	//and zero first row and column
	for(int i=0; i<n+1; i++){
		S->v[0][i] = 0;
		S->v[i][0] = 0;
	}
	
	//fill F matrix
	for(int i=1; i<n+1; i++){
		F->v[i][0] += 8*k*e(i+1,n, 0, 1);
		
		//integrate
		double x = eps/2;
		while(x < 1){
			F->v[i][0] += (5*x - 5)*e(i+1,n,0,x)*eps;
			x += eps;
		}
	}
	if(verbose) cout<<"\nOriginal S matrix:\n\n";
	if(verbose) printMatrix(S);
	
	//shift S to left
	for(int i=1; i<n+1; i++){
		for(int j=0; j<3 and j+i-1 < n+1; j++){
			S->v[i][j] = S->v[i][j+i-1];
		}
		for(int j=3; j<n+1; j++) S->v[i][j] = 0;
	}
	if(verbose) cout<<"\nShifted S matrix:\n\n";
	if(verbose) printMatrix(S);
	
	//gauss
	F->v[1][0] /= S->v[1][1];
	S->v[1][0] /= S->v[1][1];
	S->v[1][2] /= S->v[1][1];
	S->v[1][1] /= S->v[1][1];
	for(int k=2; k<=n; k++){
		double dzielnik = S->v[k][0]/S->v[k-1][1];
		S->v[k][0] -= dzielnik*S->v[k-1][1];
		S->v[k][1] -= dzielnik*S->v[k-1][2];
		
		F->v[k][0] -= dzielnik*F->v[k-1][0];
		dzielnik = S->v[k][1];
		F->v[k][0] /= dzielnik;
		
		S->v[k][0] /= dzielnik;
		S->v[k][1] /= dzielnik;
		S->v[k][2] /= dzielnik;
	}
	
	
	
	if(verbose) cout<<"\nGaussed S matrix:\n\n";
	if(verbose) printMatrix(S);
	
	//Result coefficients
	Matrix *res = zeros(n+1,1);
	res->v[n][0] = F->v[n][0];
	for(int i=n-1; i>0; i--){
		res->v[i][0] = F->v[i][0] - S->v[i][2]*res->v[i+1][0];
	}
	
	//Result points with shift revert
	int resolution = 100;
	if(argc >= 4) resolution = atoi(argv[3]);
	Matrix *plot = zeros(resolution, 2);
	for(int i=0; i<resolution; i++){
		double x = i*1.0/(resolution-1);
		plot->v[i][0] = x;
		for(int j=1; j<=n+1; j++){
			plot->v[i][1] += res->v[j-1][0] * e(j,n,0,x);
		}
		//add shift
		plot->v[i][1] += shift(x);
	}
	
	if(verbose) cout<<endl<<"Result:\n";
	printMatrix(plot);
	return 0;
}
