#include<iostream>
#include<cmath>
#include<cstdlib>
using namespace std;


struct Matrix{
    int m,n;
    double **v;
    Matrix(int m, int n){
        this->m = m;
        this->n = n;
        this->v = new double*[m];
        for(int i=0; i<m; i++){
            this->v[i] = new double[n];
            for(int j=0; j<n; j++)
                this->v[i][j] = 0;
        }
    }
};

double det(Matrix*);

void deleteMatrix(Matrix *m){
    for(int i=0; i<m->m; i++)
        delete [] m->v[i];
    delete [] m->v;
    delete m;
}

Matrix *fillMatrix(Matrix *m){
    for(int i=0; i<m->m; i++){
        cout<<"("<< m->n <<"): ";
        for(int j=0; j<m->n; j++) cin >> m->v[i][j];
    }
    return m;
}

Matrix *zeros(int m, int n){
    Matrix *res = new Matrix(m,n);
    return res;
}

Matrix *sub(Matrix *m, Matrix *n){
    if(m->m != n->m || m->n != n->n){
        cout<<"Dimensions doesn't match!\n";
        exit(1);
    }
    Matrix *res = new Matrix(m->m, m->n);
    for(int i=0; i<m->m; i++){
        for(int j=0; j<m->n; j++){
            res->v[i][j] = m->v[i][j] - n->v[i][j];
        }
    }
    return res;
}

Matrix *add(Matrix *m, Matrix *n){
    if(m->m != n->m || m->n != n->n){
        cout<<"Dimensions doesn't match!\n";
        exit(1);
    }
    Matrix *res = new Matrix(m->m, m->n);
    for(int i=0; i<m->m; i++){
        for(int j=0; j<m->n; j++){
            res->v[i][j] = m->v[i][j] + n->v[i][j];
        }
    }
    return res;
}


Matrix *unit(int m){
    Matrix *res = new Matrix(m,m);
    for(int i=0; i<m; i++){
        res->v[i][i] = 1;
    }
    return res;
}

Matrix *mul(double s, Matrix *m){
    for(int i=0; i< m->m; i++){
        for(int j=0; j< m->n; j++){
            m->v[i][j] *= s;
        }
    }
    return m;
}

void printMatrix(Matrix *m){
    for(int i=0; i<m->m; i++){
        for(int j=0; j<m->n; j++){
            cout<<m->v[i][j]<<" ";
        }
        cout<<endl;
    }
}

Matrix *mul(Matrix *m1, Matrix *m2){
    if(m1->n != m2->m) return NULL;
    int m = m1->m;
    int n = m2->n;
    Matrix *res = new Matrix(m, n);
    
    for(int i=0; i<m; i++){
        for(int j=0; j<n; j++){
            for(int k=0; k<m1->n; k++){
                res->v[i][j] += m1->v[i][k]*m2->v[k][j];
            }
        }
    }

    return res;
}

Matrix *transponse(Matrix *m){
    Matrix *res = new Matrix(m->n, m->m); //swapped (m,n)
    for(int i=0; i<m->m; i++){
        for(int j=0; j<m->n; j++){
            res->v[j][i] = m->v[i][j];
        }
    }
    return res;
}

Matrix *reverse(Matrix *m){
    double determinant = det(m);
    if(fabs(determinant) < 1e-6){
        cout<<"Can't reverse that matrix - determinant is zero!\n";
        exit(1);
    }

    if(m->m == 1)
        return mul(1/determinant,unit(1));

    Matrix *fills = new Matrix(m->m, m->n);
    int sign = 1;
    int n = m->m;
    for(int k=0; k<n; k++){
        for(int l=0; l<n ;l++){
            if((k+l) % 2 == 0) sign = 1;
            else sign = -1;
            Matrix *sub = new Matrix(n-1, n-1);
            int si = 0, sj = 0;
            for(int i=0; si< n-1; i++){
                if(i == k) continue;
                sj = 0;
                for(int j=0;sj < n-1 ; j++){
                    if(j == l) continue;
                    sub->v[si][sj] = m->v[i][j];
                    sj++;
                    
                }
                si++;
            }
            fills->v[k][l] = sign*det(sub);
            sign *= -1;
            deleteMatrix(sub);
        }
    }
    Matrix *transFills = transponse(fills);
    return mul(1/determinant, transFills);
}

double det(Matrix *m){
    if(m->m != m->n){
        cout<<"Can't calculate det of non-square matrix!\n";
        exit(1);
    }
    if(m->m == 1) return m->v[0][0];
    if(m->m == 2) return m->v[0][0]*m->v[1][1] - m->v[0][1]*m->v[1][0];
    double res = 0;
    int sign = 1;
    int n = m->m;
    for(int k=0; k<n; k++){
        Matrix *sub = new Matrix(n-1, n-1);
        int si = 0, sj = 0;
        for(int i=1; si< n-1; i++){
            sj = 0;
            for(int j=0;sj < n-1 ; j++){
                if(j == k) continue;
                sub->v[si][sj] = m->v[i][j];
                sj++;
                
            }
            si++;
        }
        res += sign*(m->v[0][k])*det(sub);
        sign *= -1;
        deleteMatrix(sub);
    }
    return res;
}

int main(){
	int n;
	double c;
	cin>>c>>n;
	double h = 1.0/n;
	Matrix *f = zeros(n-1, 1);
	Matrix *res = zeros(n-1, 1);
	double p1 = -c/h/h;
	double p2 = 2*c/h/h;
	double p3 = p1;


	//fill m
	Matrix *m = zeros(n-1, 3);
	m->v[0][1] = p2;
	m->v[0][2] = p3;

	for(int i=1; i< n-1 -1; i++){
		m->v[i][0] = p1;
		m->v[i][1] = p2;
		m->v[i][2] = p3;
	}

	m->v[n-1-1][0] = p1;
	m->v[n-1-1][1] = p2;



	//gauss
	f->v[0][0] /= m->v[0][1];
	m->v[0][2] /= m->v[0][1];
	m->v[0][1] = 1;

	for(int i=1; i<n-1-1; i++){
		double divider = m->v[i][0]/m->v[i-1][1];
		m->v[i][0] -= divider*m->v[i-1][1];
		m->v[i][1] -= divider*m->v[i-1][2];
		f->v[i][0] -= divider*f->v[i-1][0];
		f->v[i][0] /= m->v[i][1];

		m->v[i][2] /= m->v[i][1];
		m->v[i][1] = 1;
	}



	//reverse result
	res->v[n-1-1][0] = f->v[n-1-1][0];
	for(int i=n-2-1; i>=0; i--){
		res->v[i][0] -= m->v[i][2]*res->v[i+1][0];
	}

	//graph preparation
	Matrix *values = zeros(n+1, 1);
	for(int i=1; i<n - 1; i++){
		values->v[i][0] = res->v[i][0];
	}

	//return 0;
	Matrix *args = zeros(n+1, 1);
	for(int i=0; i<= n+1 - 1; i++){
		args->v[i][0] = i*h;
	}


	//dirichlet shift fix
	for(int i=0; i<= n+1 - 1; i++){
		values->v[i][0] += 1 - (i*h);
	}



	//gnuplot output
	for(int i=0; i<= n+1 - 1; i++){
		cout<<args->v[i][0]<<" "<<values->v[i][0]<<endl;
	}

	return 0;
	// ^ MRS				^
	// **********************
	// v Linear regresison	v

    // int n;
    // cout<<"How many points? ";
    // cin>>n;
    // Matrix *a = new Matrix(n,2);
    // Matrix *b = new Matrix(n,1);
    // for(int i=0; i<n; i++){
    //     a->v[i][0] = 1;
    //     cin>>a->v[i][1];

    //     cin>>b->v[i][0];
    // }

    // Matrix *at = transponse(a);
    // Matrix *left = mul(at, a);
    // Matrix *right = mul(at, b);
    // Matrix *alpha = new Matrix(2,2);
    // Matrix *beta = new Matrix(2,2);
    // for(int i=0; i<2; i++){
    //     alpha->v[i][0] = right->v[i][0];
    //     alpha->v[i][1] = left->v[i][1];

    //     beta->v[i][0] = left->v[i][0];
    //     beta->v[i][1] = right->v[i][0];
    // }
    // double resAlpha = det(alpha)/det(left);
    // double resBeta = det(beta)/det(left);
    // Matrix *xMatrix = new Matrix(2,1);
    // xMatrix->v[0][0] = resAlpha;
    // xMatrix->v[1][0] = resBeta;
    // Matrix *Ax = mul(a,xMatrix);
    // Matrix *AxMinusB = sub(Ax, b);
    // Matrix *error = mul(transponse(AxMinusB), AxMinusB);
    // cout<<"y = "<<resBeta<<" x + "<<resAlpha<<endl;
    // cout<<"Error: ";
    // printMatrix(error);
    // return 0;
}