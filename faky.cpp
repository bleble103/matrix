#include<iostream>
#include<cmath>
#include<cstdlib>
#include<cstdio>
using namespace std;

double shift(double x){
	return 5*(1-x);
}

struct Matrix{
    int m,n;
    double **v;
    Matrix(int m, int n){
        this->m = m;
        this->n = n;
        this->v = new double*[m];
        for(int i=0; i<m; i++){
            this->v[i] = new double[n];
            for(int j=0; j<n; j++)
                this->v[i][j] = 0;
        }
    }
};


Matrix *zeros(int m, int n){
    Matrix *res = new Matrix(m,n);
    return res;
}


void printMatrix(Matrix *m){
    for(int i=0; i<m->m; i++){
        for(int j=0; j<m->n; j++){
            //cout<<m->v[i][j]<<"\t";
            printf("%.4f\t",m->v[i][j]);
        }
        cout<<endl;
    }
}


double e(int i, int n, int d, double x){
	double mini = (i-2.0)/n;
	double mid = mini + 1.0/n;
	double maxi = mid + 1.0/n;
	if(x<mini or x>maxi){
		return 0;
	} 
	
	// x in range (min, max)
	if(x < mid){
		if(d == 0) return x - mini;
		
		//d == 1
		return 1;
	}
	
	//x > mid
	if(d == 0) return maxi - x;
	
	//d == 1
	return -1;
}

double eps = 1e-3;

int main(int argc, char** argv){
	int n;
	double k;
	bool verbose = true;
	if(argc >= 3){
		verbose = false;
		k = atof(argv[1]);
		n = atoi(argv[2]);
	}
	
	if(verbose){
		cout<<"Podaj k: ";
		cin>>k;
		cout<<"Podaj n: ";
		cin>>n;
	}
	
	//Matrix *S = zeros(n+1, n+1);
	Matrix *F = zeros(n+1, 1);
	
	
	Matrix *fakeS = zeros(n+1, 3);
	//first row 0
	double P1 = -k/n;
	double P2 = 2*k/n;
	double P3 = P1;
	
	fakeS->v[1][1] = P2;
	fakeS->v[1][2] = P3;
	
	//all except the last one
	for(int i=2; i<n+1 - 1; i++){
		fakeS->v[i][0] = P1;
		fakeS->v[i][1] = P2;
		fakeS->v[i][2] = P3;
	}
	
	//last row
	fakeS->v[n][0] = P1;
	fakeS->v[n][1] = k/n;
	
	
	//fill F matrix
	for(int i=1; i<n+1; i++){
		F->v[i][0] += 8*k*e(i+1,n, 0, 1);
		
		//integrate
		double x = eps/2;
		while(x < 1){
			F->v[i][0] += (5*x - 5)*e(i+1,n,0,x)*eps;
			x += eps;
		}
	}

	if(verbose) cout<<"\nShifted S matrix:\n\n";
	if(verbose) printMatrix(fakeS);
	
	//gauss
	F->v[1][0] /= fakeS->v[1][1];
	fakeS->v[1][0] /= fakeS->v[1][1];
	fakeS->v[1][2] /= fakeS->v[1][1];
	fakeS->v[1][1] /= fakeS->v[1][1];
	for(int k=2; k<=n; k++){
		double dzielnik = fakeS->v[k][0]/fakeS->v[k-1][1];
		fakeS->v[k][0] -= dzielnik*fakeS->v[k-1][1];
		fakeS->v[k][1] -= dzielnik*fakeS->v[k-1][2];
		
		F->v[k][0] -= dzielnik*F->v[k-1][0];
		dzielnik = fakeS->v[k][1];
		F->v[k][0] /= dzielnik;
		
		fakeS->v[k][0] /= dzielnik;
		fakeS->v[k][1] /= dzielnik;
		fakeS->v[k][2] /= dzielnik;
	}
	
	
	
	if(verbose) cout<<"\nGaussed S matrix:\n\n";
	if(verbose) printMatrix(fakeS);
	
	//Result coefficients
	Matrix *res = zeros(n+1,1);
	res->v[n][0] = F->v[n][0];
	for(int i=n-1; i>0; i--){
		res->v[i][0] = F->v[i][0] - fakeS->v[i][2]*res->v[i+1][0];
	}
	
	//Result points with shift revert
	int resolution = 100;
	if(argc >= 4) resolution = atoi(argv[3]);
	Matrix *plot = zeros(resolution, 2);
	for(int i=0; i<resolution; i++){
		double x = i*1.0/(resolution-1);
		plot->v[i][0] = x;
		for(int j=1; j<=n+1; j++){
			plot->v[i][1] += res->v[j-1][0] * e(j,n,0,x);
		}
		//add shift
		plot->v[i][1] += shift(x);
	}
	
	if(verbose) cout<<endl<<"Result:\n";
	printMatrix(plot);
	return 0;
}
